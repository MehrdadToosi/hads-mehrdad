package ir.toosi.miniproject4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class Activity2 extends AppCompatActivity {

    EditText txtb1;
    TextView tvb1;
    public int c;

    public int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int level;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        level = getIntent().getIntExtra("num", 0);
        if (level == 1) {
            i = 10;
        }
        if (level == 2) {
            i = 7;
        }
        if (level == 3) {
            i = 5;
        }
        
        Random rd = new Random();
        final int random = rd.nextInt(10);
        txtb1 = (EditText) findViewById(R.id.txtb1);
        tvb1 = (TextView) findViewById(R.id.tvb1);
        final Button btnb1 = (Button) findViewById(R.id.btnb1);

        btnb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int input = Integer.parseInt(txtb1.getText().toString());

                if (c < i) {
                    if (input < random) {

                        tvb1.setText("عدد بزرگتر وارد کنید");

                    } else if (input > random) {

                        tvb1.setText("عدد کوچکتر وارد کنید");

                    } else if (input == random) {

                        Toast.makeText(Activity2.this, "شما برنده شدید!!!", Toast.LENGTH_LONG).show();
                        tvb1.setText("شما برنده شدید!!!");
                    }

                    c++;
                }

                if (c >= i) {

                    Toast.makeText(Activity2.this, "تعداد دفعات حدس شما به پایان رسید.", Toast.LENGTH_LONG).show();
                    tvb1.setText("تعداد دفعات حدس شما به پایان رسید.");
                }
            }
            // }
        });

    }
}
