package ir.toosi.miniproject4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ;
        Button btna1 = (Button) findViewById(R.id.btn1);
        btna1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v1) {


                Intent intent = new Intent(MainActivity.this, Activity2.class);
                intent.putExtra("num", 1);
                startActivity(intent);

            }
        });
        Button btna2 = (Button) findViewById(R.id.btn2);
        btna2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v1) {


                Intent intent = new Intent(MainActivity.this, Activity2.class);

                intent.putExtra("num", 2);
                startActivity(intent);


            }
        });
        Button btna3 = (Button) findViewById(R.id.btn3);
        btna3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v1) {

                Intent intent = new Intent(MainActivity.this, Activity2.class);
                intent.putExtra("num", 3);
                startActivity(intent);

            }
        });

    }
}


